# 0xdf Hacks Stuff Site Nuggests

## Description

A collection of information to share about customizations I made for [0xdf.gitlab.io](https://0xdf.gitlab.io/). The site is built on Jekyll using the Minima theme, but heavily customized.

If there are other features of the site that you would be interested in seeing, raise an issue and I'll see what I can share.

## License

Code is licensed under MIT - do what you want with it.

## Items

- [Floating Table of Contents](#floating-table-of-contents)
- [Truncated Images](#truncated-images)

### Floating Table of Contents [🔗](#floating-table-of-contents)

![image-20221213113238847](assets/image-20221213113238847.png)

This is implemented from [this site](https://t.co/eso6lm5Jfx).

### Truncated Images [🔗](#truncated-images)

![image-20221213113339224](assets/image-20221213113339224.png)

This is implemented with CSS using `style="clip-path: polycon()"`. To make it scalable, I've built an include for Jekyll, `img_link`. Drop this in your `_includes` folder, and then reference it like this:

```python
{% include img_link src="/img/image-20221111145357104" alt="image-20221111145357104" ext="png" trunc=400 %}
```

This will get an image at `/img/image-20221111145357104.png` and truncate it 400 pixels down.

