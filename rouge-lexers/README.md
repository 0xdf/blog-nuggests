## Rouge Lexers

### Background

Jekyll uses [Rouge](https://github.com/rouge-ruby/rouge) for syntax highlighting on code blocks. It is possible to create custom lexers that will handle different languages.

These are Ruby scripts that will be run if kept in the `_plugins` directory in the root of the Jekyll project.

### Quality

I'm _very_ weak at Ruby, and these Lexers are hacked together at best. Please use at your own discretion, and any feedback to make them better is welcome!

### Colors

The lexer will break apart data and give HTML span objects classes. The CSS is going to be dependent on your theme. 

#### Using Theme Tokens

The `gdb-peda` lexer is still using the given theme tokens. For my theme, these are the color breakdowns:

```
      # w Text::Whitespace = grayed out
      # go Generic::Output = white
      # gr Generic::Error = red
      # sb Literal::String::Backtick = brighter red
      # gp Generic::Prompt = light green
      # sr Literal::String::Regex = green
      # n Name = yellow
      # k Keyword = yellow bold
      # kt Keyword::Type = dark blue bold
      # na Name::Attribute = teal dim
      # nb Name::Builtin = teal light
      # ni Name::Enttiy = dark pink
      # ne Name::Exception = peach bold
      # nn Name::Namespace = blue bright
      # nt Name::Tag = purple
```

You may need to adjust to make them work in your theme.

#### Custom Tokens

Newer Lexers define custom tokens within the plugin file. The text that gets matched with the given token will have the CSS class defined in the token creation. For example, for `feroxbuster`, you'll want to provide CSS for `feroxbuster-blue`, `feroxbuster-green`, etc.

### Use

The `feroxbuster` and `netexec` languages are written to be used as `output` for a `console` lexer as so:

```
```console?prompt=hacky$&output=feroxbuster
oxdf@hacky$ feroxbuster -k -u https://bizness.htb

 ___  ___  __   __     __      __         __   ___
|__  |__  |__) |__) | /  `    /  \ \_/ | |  \ |__
|    |___ |  \ |  \ | \__,    \__/ / \ | |__/ |___
by Ben "epi" Risher 🤓                 ver: 2.9.3
───────────────────────────┬──────────────────────
 🎯  Target Url            │ https://bizness.htb
...[snip]...
```

They may work as the language, but this hasn't be tested.

`peda` is focused on the `context` output of Peda. It could be updated in the future to include more. It works as a language if the block starts with the `registers` header, or as output for a `console`.