require 'rouge'

module Rouge
  module FeroxbusterTokens
    Blue = Rouge::Token.make_token('Blue', 'feroxbuster-blue')
    Green = Rouge::Token.make_token('Green', 'feroxbuster-green')
    Yellow = Rouge::Token.make_token('Yellow', 'feroxbuster-yellow')
    Red = Rouge::Token.make_token('Red', 'feroxbuster-red')
    Cyan = Rouge::Token.make_token('Cyan', 'feroxbuster-cyan')
  end

  module Lexers
    class Feroxbuster < RegexLexer
      tag 'feroxbuster'
      title "Feroxbuster"
      desc "Lexer for feroxbuster output"



      state :root do
        rule %r/\s+/m, Generic::Output
        rule %r/4\d\d/, FeroxbusterTokens::Red, :status_results
        rule %r/3\d\d/, FeroxbusterTokens::Yellow, :status_results
        rule %r/2\d\d/, FeroxbusterTokens::Green, :status_results
        rule %r/WLD/, FeroxbusterTokens::Cyan, :status_results
        rule %r/\A\[/ do
          token Generic::Output
          goto :progress1
        end
        rule /.+/, Generic::Output
      end

      state :status_results do
        rule %r/\s+/, Generic::Output
        rule %r/4\d\d\b/, FeroxbusterTokens::Red
        rule %r/3\d\d\b/, FeroxbusterTokens::Yellow
        rule %r/2\d\d\b/, FeroxbusterTokens::Green
        rule %r/WLD/, FeroxbusterTokens::Cyan
        rule %r/=> /, Generic::Output, :redirect
        rule %r/Auto-filtering /, FeroxbusterTokens::Green
        rule %r/--[\w-]+/, FeroxbusterTokens::Yellow
        rule %r/\A\[/ do
          token Generic::Output
          goto :progress1
        end
        rule /\S+/, Generic::Output
      end

      state :redirect do
        rule %r/.+/, FeroxbusterTokens::Yellow, :pop!
      end

      state :progress1 do
        rule %r/\s*\n/ do
          token Generic::Output
          goto :progress2
        end
        rule %r/\[/, Generic::Output 
        rule %r/#+/, FeroxbusterTokens::Yellow
        rule %r/found/, FeroxbusterTokens::Green
        rule %r/errors/, FeroxbusterTokens::Red
        rule %r/\s+/, Generic::Output
        rule %r/\S+/, Generic::Output
      end

      state :progress2 do
        rule %r/\[/, Generic::Output 
        rule %r/#+/, FeroxbusterTokens::Cyan
        rule %r/found/, FeroxbusterTokens::Green
        rule %r/errors/, FeroxbusterTokens::Red
        rule %r/\s+/, Generic::Output
        rule %r/\S+/, Generic::Output
      end
    end
  end
end
