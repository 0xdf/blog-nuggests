require 'rouge'

module Rouge
  module Lexers
    class GdbPeda < RegexLexer
      tag 'peda'
      title "GDB PEDA"
      desc "Lexer for GDB PEDA output"

      # kt Keyword::Type = dark blue bold
      # sr Literal::String::Regex = green
      # gr Generic::Error = red
      # go Generic::Output = white
      # gp Generic::Prompt = light green


      state :root do
        rule /\s+/m, Generic::Output
        rule /...\[snip\].../, Comment
        rule %r/^RAX|RDX|RCX|RBX|RDI|RSI|RSP|RBP|RIP|R8|R9|R10|R11|R12|R13|R14|R15|EFLAGS/, Literal::String::Regex
        rule /^\[-+registers-+\]$/, Keyword::Type, :registers
        rule /^\[-+code-+\]$/, Keyword::Type, :code
        rule /^\[-+stack-+\]$/, Keyword::Type, :stack
        rule /^\[-+\]$/, Keyword::Type
        rule %r/^Legend: /, Generic::Output, :legend
        rule %r/^Stopped reason: /, Generic::Output, :stopped
        rule /.+/, Generic::Output
      end

      state :registers do
        rule /...\[snip\].../, Comment
        rule %r/\n(?=\[--------)/m, Generic::Output, :pop!
        rule %r/^EFLAGS:/, Literal::String::Regex, :eflags
        rule %r/^[^:]+/, Literal::String::Regex
        rule %r/ --> /, Generic::Output
        rule %r/\(.*\)/, Generic::Output
        rule %r/0x[0-9a-fA-F]+(?=\n)/i, Generic::Output
        rule %r/0x[0-9a-fA-F]+(?= \(["'])/i, Keyword::Type
        rule %r/0x[0-9a-fA-F]+(?= \()/i, Generic::Error
        rule %r/0x[0-9a-fA-F]+(?= -->)/i, Keyword::Type
        rule %r/[:\s]+/, Generic::Output
        rule %r/\n/, Generic::Output
        rule %r/.+/, Generic::Output
      end

      state :eflags do
        rule %r/\s?0x[0-9a-fA-F]+/, Generic::Output
        rule %r/ \(/, Generic::Output
        rule %r/[a-z]+/, Literal::String::Regex
        rule %r/[A-Z]+/, Generic::Error
        rule %r/ /, Generic::Output
        rule %r/\)/ do
          token Generic::Output
          goto :root
        end
      end

      state :code do
        rule /...\[snip\].../, Comment
        rule %r/\n(?=\[--------)/m, Generic::Output, :pop!
        rule /^\[-+stack-+\]$/ do
          token Keyword::Type
          goto :stack
        end
        rule %r/^Invalid \$PC address: 0x\d+/, Generic::Output, :pop!
        rule %r/^=>.+:\s+/ do
          token Generic::Output
          goto :code_active
        end
        rule %r/^.+:\s+/, Generic::Output
        rule %r/.+/, Text::Whitespace
        rule %r/\n/, Generic::Output
      end

      state :code_active do
        rule %r/Guessed arguments:\n/ do
          token Generic::Output
          goto :stack
        end
        rule /^\[-+stack-+\]$/ do
          token Keyword::Type
          goto :stack
        end
        rule /...\[snip\].../, Comment
        rule %r/\n(?=\[--------)/m, Generic::Output, :pop!
        rule %r/^.+:\s+/, Generic::Output
        rule %r/.+/, Literal::String::Regex
        rule %r/\n/, Generic::Output
      end

      # also covers guessed args
      state :stack do
        rule /...\[snip\].../, Comment
        rule %r/^\[-+stack-+\]$/, Keyword::Type
        rule %r/^\[-+\]\s*$/ do
          token Keyword::Type
          goto :legend
        end
        rule %r/\n(?=\[--------)/m, Generic::Output, :pop!
        rule %r/\d{4,}\| /, Generic::Output
        rule %r/ --> /, Generic::Output
        rule %r/\s+\(.*\)/, Generic::Output
        rule %r/0x[0-9a-fA-F]+(?=\s+\n)/i, Generic::Output
        rule %r/0x[0-9a-fA-F]+(?= \(["'])/i, Keyword::Type
        rule %r/0x[0-9a-fA-F]+(?= \()/i, Generic::Error
        rule %r/0x[0-9a-fA-F]+(?= -->)/i, Keyword::Type
        rule %r/arg\[\d+\]: /, Generic::Output
        rule %r/\n/, Generic::Output
      end

      state :legend do
        rule /...\[snip\].../, Comment
        rule %r/\n/, Generic::Output, :pop!
        rule %r/, /, Generic::Output
        rule %r/code/, Generic::Error
        rule %r/data/, Keyword::Type
        rule %r/rodata/, Literal::String::Regex
        rule %r/value/, Generic::Output
      end

      state :stopped do
        rule /...\[snip\].../, Comment
        rule %r/.+/, Generic::Error, :pop!
      end
    end
  end
end
