require 'rouge'

module Rouge
  module NetExecTokens
    Protocol = Rouge::Token.make_token('Protocol', 'netexec-protocol')
    LogMessage = Rouge::Token.make_token('LogMessage', 'netexec-logmessage')
    LogFail = Rouge::Token.make_token('LogFail', 'netexec-logfail')
    LogSuccess = Rouge::Token.make_token('LogSuccess', 'netexec-logsuccess')
    Pwned = Rouge::Token.make_token('Pwned', 'netexec-pwned')
    ShareEnum = Rouge::Token.make_token('ShareEnum', 'netexec-shareenum')
  end

  module Lexers
    class NetExec < RegexLexer
      tag 'netexec'
      aliases 'crackmapexec'
      title "NetExec"
      desc "Lexer for NetExec output"

      protocols = %w(SMB WINRM LDAP\-CHE\.\.\. LDAPS FTP RDP SSH MSSQL VNC WMI HTTP HTTPS MAQ LDAP SPIDER_P\.\.\. CME ADCS)

      state :root do
        rule %r/\s+/, Generic::Output
        rule %r/SMBv1:False/, Name::Builtin
        rule %r/(#{protocols.join('|')}) /, NetExecTokens::Protocol
        rule %r/\[\*\]/, NetExecTokens::LogMessage
        rule %r/\[-\]/, NetExecTokens::LogFail
        rule %r/\[\+\]/, NetExecTokens::LogSuccess
        rule %r/\(Pwn3d\!\)/, NetExecTokens::Pwned
        rule %r/\(/, Generic::Output
        rule %r/signing:True/, NetExecTokens::LogSuccess
        rule %r/\)/, Generic::Output
        rule %r/Share\s+Permissions.+/, NetExecTokens::ShareEnum, :share_lines
        rule %r/\S+/, Generic::Output
      end

      state :share_lines do
        rule %r/(#{protocols.join('|')}) /, NetExecTokens::Protocol
        rule %r/\s*\S+\s+\d+\s+\S+\s+(?=\[)/, Generic::Output, :pop!
        rule %r/\s*\S+\s+\d+\s+\S+\s+(?!\[)/, Generic::Output, :share_permissions
      end

      state :share_permissions do
        rule %r/.+\n/, NetExecTokens::ShareEnum, :pop!
      end
    end
  end
end
